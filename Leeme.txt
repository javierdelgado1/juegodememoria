Este es un b�sico juego de memoria hecho en HTML5

-1ero se da doble click al archivo index el cual es un archivo del tipo html.

-Se observar� una ventana emergente interna el cual pide que se haga click en iniciar para comenzar a jugar.

-Al comenzar este tendr� un temporizador,tambi�n se observar� un contador clicks y los botones Pausar/Continuar y Reiniciar.

-La explicaci�n del juego es muy b�sica, solo consiste en buscar el par de la imagen que est� bajo los recuadros negros

-Se le da click en el recuadro para ver la imagen y luego click en el otro recuadro hasta encontrar el par correspondiente
a la imagen.

-Si desea puede dar click al bot�n pausar para hace una parada en el juego o dar click en el bot�n reiniciar en caso de que
quiera volver a comenzar.

-Si paus� el juego y lo quiere retomar solo debe presionar el bot�n continuar y continuar� el juego.

-Si reinici� el juego y quiere iniciar solo debe presionar el bot�n Iniciar y comenzar� el juego desde cero.